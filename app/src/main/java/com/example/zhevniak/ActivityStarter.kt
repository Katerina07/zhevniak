package com.example.zhevniak

import com.example.zhevniak.App
import com.example.zhevniak.DesktopItem

interface ActivityStarter {
fun startActivity(app: App)
    fun startDesktop(desktopItem: DesktopItem)
}
