package com.example.zhevniak.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.widget.Toolbar
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.zhevniak.R
import com.example.zhevniak.custom.CustomApplication
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_launcher.*
import kotlinx.android.synthetic.main.activity_launcher_kate.*
import kotlinx.android.synthetic.main.fragment_settings.*

class LauncherActivity : AppCompatActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)

        val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        if (prefs.getBoolean("NEW_APP", true)) {
            startActivity(Intent(this, WelcomePage::class.java))
        }

        val sharedPrefs = getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)



        val mode = if (sharedPrefs.getBoolean("dark_theme", false)) {
            AppCompatDelegate.MODE_NIGHT_YES
        } else {
            AppCompatDelegate.MODE_NIGHT_NO
        }
        AppCompatDelegate.setDefaultNightMode(mode)

        setContentView(R.layout.activity_launcher)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val fab: FloatingActionButton = findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            val snackbar = Snackbar.make(view, "Удалить элемент??", Snackbar.LENGTH_LONG)
            val snackbarView = snackbar.view
            snackbar.setActionTextColor(resources.getColor(R.color.Background))
            snackbarView.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            snackbar.setAction("Удалить") { Log.i("Snackbar", "Кот покормлен") }
                    .addCallback(object : Snackbar.Callback() {
                        override fun onShown(sb: Snackbar?) {
                            Log.i("Snackbar", "Показан")
                        }

                        override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                            when(event) {
                                DISMISS_EVENT_TIMEOUT -> Log.i("Snackbar", "Закрыт по таймауту")
                                DISMISS_EVENT_ACTION -> {
                                    // )))0)
                                    Log.i("Snackbar", "Закрыт по экшену")
                                }
                                DISMISS_EVENT_MANUAL -> Log.i("Snackbar", "Закрыт вручную")
                                DISMISS_EVENT_SWIPE -> Log.i("Snackbar", "Закрыт свайпом")
                            }
                        }
                    })


                    .show()
        }

        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        appBarConfiguration = AppBarConfiguration(
                setOf(
                        R.id.launcherFragment, R.id.listFragment, R.id.settingsFragment
                ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.launcher_activity_menu, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }
}
