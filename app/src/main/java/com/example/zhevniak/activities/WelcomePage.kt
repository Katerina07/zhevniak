package com.example.zhevniak.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.os.bundleOf
import androidx.navigation.Navigation
import androidx.viewpager.widget.ViewPager
import com.example.zhevniak.LauncherActivity
import com.example.zhevniak.MainFragment
import com.example.zhevniak.R
import com.example.zhevniak.adapters.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_welcome_page.*


class WelcomePage : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {

        val sharedPrefs = getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)
        val mode = if (sharedPrefs.getBoolean("dark_theme", false))
            AppCompatDelegate.MODE_NIGHT_YES
        else
            AppCompatDelegate.MODE_NIGHT_NO

        AppCompatDelegate.setDefaultNightMode(mode)

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_page)

        choiceActivity()
    }

    private fun choiceActivity() {

        val adapter = ViewPagerAdapter(supportFragmentManager)
        viewPager?.adapter = ViewPagerAdapter(supportFragmentManager)


        viewPager?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
            ) {

                val lastIdx: Int = adapter.count - 2
                if (position == lastIdx && positionOffsetPixels > 6) {
                    setResult(RESULT_OK)
                    finish()
                    //startActivity(Intent(applicationContext, LauncherActivity::class.java))

//                    val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
//                    prefs.edit().apply {
//                        putBoolean("NEW_APP", false)
//                        apply()
//                    }
//                    finishAffinity()
                }
            }

            override fun onPageSelected(position: Int) {
            }
        })

        next.setOnClickListener{
            if (viewPager.currentItem < 3)
                viewPager.arrowScroll(ViewPager.FOCUS_RIGHT)
            else {
            setResult(RESULT_OK)
            finish()
                //startActivity(Intent(this, LauncherActivity::class.java))
            }
        }
    }
}
