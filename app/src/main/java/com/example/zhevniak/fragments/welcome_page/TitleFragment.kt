package com.example.zhevniak.fragments.welcome_page

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.navigation.Navigation.findNavController
import com.example.zhevniak.R
import com.example.zhevniak.activities.WelcomePage
import kotlinx.android.synthetic.main.activity_welcome_page.*

class TitleFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_title,
                container, false)
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        next.setOnClickListener { findNavController(it).navigate(R.id.nextFragment) }
//    }
}
