package com.example.zhevniak.fragments.launcher_page

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.zhevniak.R
import com.example.zhevniak.adapters.RecycleViewAdapter
import com.example.zhevniak.classes.AppItem
import kotlinx.android.synthetic.main.fragment_launcher.*
import kotlinx.android.synthetic.main.fragment_list.*

class ListFragment : Fragment() {
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        viewManager = LinearLayoutManager(activity?.applicationContext)

        val dataSet = mutableListOf<AppItem>()

        val packageManager = activity!!.packageManager
        val i = Intent(Intent.ACTION_MAIN, null)
        i.addCategory(Intent.CATEGORY_LAUNCHER)

        val availableActivities = packageManager.queryIntentActivities(i, 0)
        for (resolveInfo in availableActivities) {
            dataSet.add(AppItem(
                    resolveInfo.activityInfo.loadIcon(packageManager),
                    resolveInfo.loadLabel(packageManager),
                    resolveInfo.activityInfo.name
            ))
        }

        viewAdapter = RecycleViewAdapter(dataSet, true)
        list_recycler_view.apply {
            adapter = viewAdapter
            layoutManager = viewManager
        }
    }
}
