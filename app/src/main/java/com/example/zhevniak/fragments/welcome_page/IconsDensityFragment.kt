package com.example.zhevniak.fragments.welcome_page

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.example.zhevniak.R
import com.example.zhevniak.R.layout.fragment_icons_density
import com.example.zhevniak.classes.TightLayoutSettings
import com.example.zhevniak.custom.CustomApplication.Companion.sharedPreferencesFile
import kotlinx.android.synthetic.main.fragment_icons_density.*


class IconsDensityFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(fragment_icons_density, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        if(TightLayoutSettings.layoutMode == TightLayoutSettings.Companion.LAYOUT.TIGHT){
            tightLayout?.setBackgroundResource(R.drawable.radio_button_checked)
            standardLayout?.setBackgroundResource(R.drawable.radio_button_unchecked)
            tightButton?.isChecked = true
        }
        else{
            tightLayout?.setBackgroundResource(R.drawable.radio_button_unchecked)
            standardLayout?.setBackgroundResource(R.drawable.radio_button_checked)
            standardButton?.isChecked = true
        }

        standardLayout.setOnClickListener(clickListener)
        standardButton.setOnClickListener(clickListener)
        tightLayout.setOnClickListener(clickListener)
        tightButton.setOnClickListener(clickListener)
    }

    private val clickListener = View.OnClickListener {

        val sharedPref = activity?.getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)
        val editor = sharedPref?.edit()

        when (it.id) {
            R.id.standardLayout, R.id.standardButton ->
            {
                standardButton?.isChecked = true
                tightButton?.isChecked = false
                tightLayout?.setBackgroundResource(R.drawable.radio_button_unchecked)
                standardLayout?.setBackgroundResource(R.drawable.radio_button_checked)
                TightLayoutSettings.layoutMode = TightLayoutSettings.Companion.LAYOUT.STANDARD

                if (editor != null) {
                    editor.putBoolean("tight_layout", false)
                    editor.apply()
                }
            }
            R.id.tightLayout, R.id.tightButton ->
            {
                standardButton?.isChecked = false
                tightButton?.isChecked = true
                tightLayout?.setBackgroundResource(R.drawable.radio_button_checked)
                standardLayout?.setBackgroundResource(R.drawable.radio_button_unchecked)
                TightLayoutSettings.layoutMode = TightLayoutSettings.Companion.LAYOUT.TIGHT

                if (editor != null) {
                    editor.putBoolean("tight_layout", true)
                    editor.apply()
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
//        val prefs = activity?.getSharedPreferences(sharedPreferencesFile, Context.MODE_PRIVATE)
//        val editor =  prefs?.edit()
//        if(TightLayoutSettings.layoutMode == TightLayoutSettings.Companion.LAYOUT.TIGHT){
//            editor?.putBoolean("tight_layout", true)
//        }
//        else{
//            editor?.putBoolean("tight_layout", false)
//        }
//        editor?.apply()
    }
}
//        standardLayout?.setOnClickListener{
//            standardButton?.isChecked = true
//            tightButton?.isChecked = false
//            tightLayout?.setBackgroundResource(R.drawable.radio_button_unchecked)
//            standardLayout?.setBackgroundResource(R.drawable.radio_button_checked)
//            layoutMode = TightLayoutSettings.Companion.LAYOUT.STANDARD
//        }
//        tightLayout?.setOnClickListener{
//            standardButton?.isChecked = false
//            tightButton?.isChecked = true
//            tightLayout?.setBackgroundResource(R.drawable.radio_button_checked)
//            standardLayout?.setBackgroundResource(R.drawable.radio_button_unchecked)
//            layoutMode = TightLayoutSettings.Companion.LAYOUT.TIGHT
//
//        }

//    override fun onDestroyView() {
//        TightLayoutSettings.LAYOUT = layoutMode
//        IconsDensityFragmentDirections.nextFragment().setTight_layout(layoutMode)
//        super.onDestroyView()
//    }