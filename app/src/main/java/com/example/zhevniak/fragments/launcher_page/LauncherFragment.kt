package com.example.zhevniak.fragments.launcher_page

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.media.VolumeShaper
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.zhevniak.R
import com.example.zhevniak.adapters.RecycleViewAdapter
import com.example.zhevniak.classes.AppItem
import com.example.zhevniak.custom.CustomApplication
import kotlinx.android.synthetic.main.fragment_launcher.*


class LauncherFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        val packageManager = activity?.packageManager
//        LoadApplications().execute()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_launcher, container, false)


//        viewAdapter = RecycleViewAdapter()
//
//        recyclerView = findViewById<RecyclerView>(R.id.my_recycler_view).apply {
//            // use this setting to improve performance if you know that changes
//            // in content do not change the layout size of the RecyclerView
//            setHasFixedSize(true)
//
//            // use a linear layout manager
//            layoutManager = viewManager
//
//            // specify an viewAdapter (see also next example)
//            adapter = viewAdapter
//
//        }
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val prefs = activity?.getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)
        val tightLayout = prefs?.getBoolean("tight_layout", false)
//        val darkTheme = prefs?.getBoolean("dark_theme", false)

        val colAmount = if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            if (tightLayout == true)
                7
            else
                6
        } else {
            if (tightLayout == true)
                5
            else
                4
        }

        viewManager = GridLayoutManager(activity?.applicationContext, colAmount)

        val dataSet = mutableListOf<AppItem>()

        val packageManager = activity!!.packageManager
        val i = Intent(Intent.ACTION_MAIN, null)
        i.addCategory(Intent.CATEGORY_LAUNCHER)

        val availableActivities = packageManager.queryIntentActivities(i, 0)
        for (resolveInfo in availableActivities) {
            dataSet.add(AppItem(
                    resolveInfo.activityInfo.loadIcon(packageManager),
                    resolveInfo.loadLabel(packageManager),
                    resolveInfo.activityInfo.name
            ))
        }

        viewAdapter = RecycleViewAdapter(dataSet, false)
        launcher_recycler_view.apply {
            adapter = viewAdapter
            layoutManager = viewManager
        }
    }

    //    protected fun onListItemClick(l: ListView?, v: View?, position: Int, id: Long) {
//        super.onListItemClick(l, v, position, id)
//        val app: ApplicationInfo = applist!![position]
//        try {
//            val intent = packageManager!!.getLaunchIntentForPackage(app.packageName)
//            intent?.let { startActivity(it) }
//        } catch (e: ActivityNotFoundException) {
//            Toast.makeText(this@MainActivity, e.message, Toast.LENGTH_LONG).show()
//        } catch (e: Exception) {
//            Toast.makeText(this@MainActivity, e.message, Toast.LENGTH_LONG).show()
//        }
//    }

//    private fun checkForLaunchIntent(list: List<*>): List<*>? {
//        val appList = ArrayList<Any>()
//        for (info in list) {
//            try {
//                if (packageManager!!.getLaunchIntentForPackage(info.packageName) != null) {
//                    appList.add(info)
//                }
//            } catch (e: Exception) {
//                e.printStackTrace()
//            }
//        }
//        return appList
//    }

//    private class LoadApplications : AsyncTask<Void?, Void?, Void?>() {
//        private var progress: ProgressDialog? = null
//        override fun doInBackground(vararg params: Void): Void? {
//            applist = checkForLaunchIntent(packageManager.getInstalledApplications(PackageManager.GET_META_DATA))
//            listadapter = AppAdapter(this@MainActivity, R.layout.list_item, applist)
//            return null
//        }
//
//        override fun onPostExecute(result: Void?) {
//            setListAdapter(listadapter)
//            progress!!.dismiss()
//            super.onPostExecute(result)
//        }
//
//        override fun onPreExecute() {
//            progress = ProgressDialog.show(this@MainActivity, null, "Loading apps info...")
//            super.onPreExecute()
//        }
//    }

}
