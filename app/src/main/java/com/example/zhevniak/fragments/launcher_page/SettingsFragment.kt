package com.example.zhevniak.fragments.launcher_page

import android.content.Context
import android.os.Bundle
import android.preference.PreferenceFragment
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatDelegate

import com.example.zhevniak.R
import com.example.zhevniak.classes.TightLayoutSettings
import com.example.zhevniak.custom.CustomApplication
import kotlinx.android.synthetic.main.fragment_settings.*

@Suppress("DEPRECATION")
class SettingsFragment: Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val prefs = activity?.getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)
        val editor =  prefs?.edit()

        switchLayout.isChecked = prefs?.getBoolean("tight_layout", false) ?: false

        switchMode.isChecked = (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES)

        switchLayout.setOnCheckedChangeListener { _: CompoundButton, _: Boolean ->
            if (editor != null) {
                editor.putBoolean("tight_layout", switchLayout.isChecked)
                editor.apply()
                editor.commit()
            }
        }

        switchMode.setOnCheckedChangeListener { _: CompoundButton, _: Boolean ->
            run {
                editor?.putBoolean("dark_theme", switchMode.isChecked)
                editor?.apply()
                editor?.commit()
            }
            if (switchMode.isChecked)
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            else
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
    }
}
