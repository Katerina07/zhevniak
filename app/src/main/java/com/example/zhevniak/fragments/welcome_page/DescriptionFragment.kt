package com.example.zhevniak.fragments.welcome_page

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.zhevniak.R
import kotlinx.android.synthetic.main.activity_welcome_page.*

class DescriptionFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater.inflate(R.layout.fragment_description,
                container, false)
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        next.setOnClickListener { Navigation.findNavController(it).navigate(R.id.descriptionFragment) }
//    }

}
