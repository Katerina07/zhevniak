package com.example.zhevniak.fragments.welcome_page


import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import com.example.zhevniak.R
import com.example.zhevniak.custom.CustomApplication
import com.example.zhevniak.custom.CustomApplication.Companion.sharedPreferencesFile
import kotlinx.android.synthetic.main.fragment_theme.*


class ThemeFragment : Fragment() {

    lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_theme, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {


        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO) {
            radioButton_lightTheme?.isChecked = true
            radioButton_darkTheme?.isChecked = false
            lightTheme?.setBackgroundResource(R.drawable.radio_button_checked)
            darkTheme?.setBackgroundResource(R.drawable.radio_button_unchecked)
        }
        else {
            radioButton_darkTheme?.isChecked = true
            radioButton_lightTheme?.isChecked = false
            lightTheme?.setBackgroundResource(R.drawable.radio_button_unchecked)
            darkTheme?.setBackgroundResource(R.drawable.radio_button_checked)
        }

        lightTheme?.setOnClickListener(clickListener)
        radioButton_lightTheme?.setOnClickListener(clickListener)
        darkTheme?.setOnClickListener(clickListener)
        radioButton_darkTheme?.setOnClickListener(clickListener)
    }

    private val clickListener = View.OnClickListener {

        val sharedPref = activity?.getSharedPreferences("MySharedPreferences", Context.MODE_PRIVATE)
        val editor = sharedPref?.edit()

        when (it.id) {
            R.id.lightTheme, R.id.radioButton_lightTheme ->
            {
                if (editor != null) {
                    editor.putBoolean("dark_theme", false)
                    editor.apply()
                }

//                sharedPreferences.edit().putBoolean("theme_dark", false).apply()
                radioButton_lightTheme?.isChecked = true
                radioButton_darkTheme?.isChecked = false
                CustomApplication.switchTheme(AppCompatDelegate.MODE_NIGHT_NO)
            }
            R.id.darkTheme, R.id.radioButton_darkTheme ->
            {
                if (editor != null) {
                    editor.putBoolean("dark_theme", true)
                    editor.apply()
                }

//                sharedPreferences.edit().putBoolean("theme_dark", true).apply()
                radioButton_darkTheme?.isChecked = true
                radioButton_lightTheme?.isChecked = false
                CustomApplication.switchTheme(AppCompatDelegate.MODE_NIGHT_YES)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
//        val prefs = activity?.getSharedPreferences(sharedPreferencesFile, MODE_PRIVATE)
//        val editor =  prefs?.edit()
//        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_NO) {
//            editor?.putBoolean("night_mode", false)
//        }
//        else{
//            editor?.putBoolean("night_mode", true)
//        }
//        editor?.apply()
    }
}

