package com.example.zhevniak.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.zhevniak.fragments.launcher_page.LauncherFragment
import com.example.zhevniak.fragments.welcome_page.DescriptionFragment
import com.example.zhevniak.fragments.welcome_page.TitleFragment
import com.example.zhevniak.fragments.welcome_page.ThemeFragment
import com.example.zhevniak.fragments.welcome_page.IconsDensityFragment

class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val fragments = arrayOf(
            TitleFragment(),
            DescriptionFragment(),
            ThemeFragment(),
            IconsDensityFragment(),
            TitleFragment()
    )

    private val title = arrayOf(
            "Welcome",
            "Description",
            "ThemeSelection",
            "IconsDensitySelection",
            "Go to Launcher"
    )

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int = fragments.size

    override fun getPageTitle(position: Int): CharSequence? {
        return title[position]
    }
}