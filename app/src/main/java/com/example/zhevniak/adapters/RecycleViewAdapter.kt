package com.example.zhevniak.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.zhevniak.R
import com.example.zhevniak.classes.AppItem
import kotlin.random.Random

class RecycleViewAdapter(private val dataSet: MutableList<AppItem>, private val LIST_LAYOUT_MANAGER: Boolean) :
        RecyclerView.Adapter<RecycleViewAdapter.MyViewHolder>() {

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val appName = view.findViewById<TextView>(R.id.app_name) as TextView
        val appIcon = view.findViewById<ImageView>(R.id.app_icon) as ImageView
        val appDescr = view.findViewById<TextView>(R.id.app_description) as TextView?
    }

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): MyViewHolder {
        val view = if(LIST_LAYOUT_MANAGER) {
            LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_view_item, parent, false)
        }
        else {
            LayoutInflater.from(parent.context)
                    .inflate(R.layout.recycle_view_item, parent, false)
        }
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.appName.text = dataSet[position].appName
        holder.appIcon.background = dataSet[position].appIcon
        if(holder.appDescr != null) {
            holder.appDescr.text = dataSet[position].appDescription
        }
    }

    override fun getItemCount() = dataSet.size
}

