package com.example.zhevniak

import android.app.Activity
import android.content.*
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.provider.Settings
import android.text.InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.SearchView
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.MenuItemCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.viewpager.widget.ViewPager
import com.example.zhevniak.activities.WelcomePage
import com.example.zhevniak.fragments.launcher_page.LauncherFragment
import com.example.zhevniak.profile.ProfileActivity
import com.example.zhevniak.wallpaper.Wallpaper
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar

import com.yandex.metrica.YandexMetrica
import kotlinx.android.synthetic.main.activity_launcher.*
import kotlinx.android.synthetic.main.content_list.*
import kotlinx.android.synthetic.main.fragment_main.*

val MAX_COUNT = 9

class LauncherActivity : AppCompatActivity(), MenuItemCompat.OnActionExpandListener,
    SearchView.OnQueryTextListener, View.OnClickListener {
    private lateinit var selectedDesktop: DesktopItem

    var needHints = false
    val WELCOME_ACTIVITY: Int = 420
    var startSearchScreen = 0;
    val mainFragment = MainFragment()
    val gridListFragment = GridListFragment()
    private lateinit var mSearchView: SearchView
    private lateinit var mSearch: MenuItem
    val DELETE_APP = 101
    val ABOUT_APP = 102
    val OPEN_COUNT = 103
    val REMOVE_APP = 104
    val ADD_TO_DESKTOP = 105

    val activityStarter = MyActivityStarter(this)
    val model: AppsViewModel by viewModels()
    val LAUNCHER_ACTIVITY_TAG = "Launcher activity"
    lateinit var mReceiver: AppsChangedReceiver;
    val intentFilterAppChanged = IntentFilter()

    val observer = Observer<Bitmap?> { bitmap ->
        if (bitmap != null) {
            findViewById<ConstraintLayout>(R.id.main).background = BitmapDrawable(bitmap)
        } else {
            findViewById<ConstraintLayout>(R.id.main).background = null
        }

    }

    private lateinit var mPager: ViewPager

    private lateinit var selectedApp: App

    private var isFabOpen = false
    private lateinit var fab: FloatingActionButton
    private lateinit var fabProfile: FloatingActionButton
    private lateinit var fabAdd: FloatingActionButton
    private lateinit var fab_open: Animation
    private lateinit var fab_close: Animation
    private lateinit var rotate_forward: Animation
    private lateinit var rotate_backward: Animation

    private inner class ScreenSlidePagerAdapter(fm: FragmentManager) :
        FragmentStatePagerAdapter(fm) {
        override fun getCount(): Int = 2
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> mainFragment
                1 -> gridListFragment
                else -> MainFragment()
            }
        }
    }

    inner class MyActivityStarter(val context: Context) : ActivityStarter {

        override fun startActivity(application: App) {
            //YandexMetrica.activate(context, LAUNCHER_ACTIVITY_TAG)

            val currentDateandTime = System.currentTimeMillis()
            application.lastLaunch = currentDateandTime
            application.launchCount++
            model.updateApp(application)
            val eventParameters = "{\"ActivityStarter\":\"start app ${application.packageName}\"}"
            YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
            context.startActivity(context.packageManager.getLaunchIntentForPackage(application.packageName))
        }

        override fun startDesktop(desktopItem: DesktopItem) {
            if (desktopItem is LinkDesktop) {
                if (!(desktopItem.url.startsWith("http://") or desktopItem.url.startsWith("https://")))
                    desktopItem.url = "http://" + desktopItem.url
                val address = Uri.parse(desktopItem.url)
                val i = Intent(Intent.ACTION_VIEW, address)
                context.startActivity(i)

            } else {
                if (desktopItem is AppDesktop) {
                    startActivity(desktopItem.app)
                }
            }
        }


    }

    lateinit var sharedPreferences: SharedPreferences;

    override fun onCreate(savedInstanceState: Bundle?) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        val nightTheme = sharedPreferences.getBoolean("dark_theme", false)
        val eventParameters = "{\"OnCreate()\":\"NightTheme:$nightTheme\"}"
        YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
        AppCompatDelegate.setDefaultNightMode(
            if (nightTheme) {
                AppCompatDelegate.MODE_NIGHT_YES
            } else {
                AppCompatDelegate.MODE_NIGHT_NO
            }
        )
        if (sharedPreferences.getBoolean("settingComplete", true)) {
            val eventParameters = "{\"OnCreate()\":\"GoWelcome\"}"
            YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
            val intent = Intent(this, WelcomePage::class.java)
            startActivityForResult(intent, WELCOME_ACTIVITY)
            needHints = true
        }
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_launcher)

        mPager = findViewById(R.id.pager)

        // The pager adapter, which provides the pages to the view pager widget.
        val pagerAdapter = ScreenSlidePagerAdapter(supportFragmentManager)
        mPager.adapter = pagerAdapter
        setSupportActionBar(toolbar)

        intentFilterAppChanged.addAction(Intent.ACTION_PACKAGE_ADDED)
        intentFilterAppChanged.addAction(Intent.ACTION_PACKAGE_REMOVED)
        intentFilterAppChanged.addDataScheme("package")
        mReceiver = AppsChangedReceiver(model)

        registerReceiver(mReceiver, intentFilterAppChanged)



        fab = findViewById<FloatingActionButton>(R.id.fab);
        fabProfile = findViewById<FloatingActionButton>(R.id.fab1);
        fabAdd = findViewById<FloatingActionButton>(R.id.fab2);
        fab_open = AnimationUtils.loadAnimation(this, R.anim.fab_open);
        fab_close = AnimationUtils.loadAnimation(this, R.anim.fab_close);
        rotate_forward = AnimationUtils.loadAnimation(this, R.anim.rotate_forward);
        rotate_backward = AnimationUtils.loadAnimation(this, R.anim.rotate_backward);
        fab.setOnClickListener(this);
        fab1.setOnClickListener(this);
        fab2.setOnClickListener(this);
    }

    fun addLink(index: Int) {
        val dialog = AlertDialog.Builder(this);
        val editText = EditText(this)
        editText.inputType = TYPE_TEXT_VARIATION_WEB_EDIT_TEXT
        dialog.setIcon(R.mipmap.ic_launcher_round);
        dialog.setTitle(getString(R.string.get_webadress));
        dialog.setView(editText);


        dialog.setPositiveButton(getString(R.string.ok),
            DialogInterface.OnClickListener { dialog, which ->
                val newLink = LinkDesktop(
                    editText.text.toString(),
                    null,
                    editText.text.toString(),
                    index
                )
                ImageGetter.getImage(
                    newLink.url, this, newLink,
                    model.getDesktopApps() as MutableLiveData<List<DesktopItem>>
                )
                model.addToDesktop(
                    newLink
                )
                dialog.dismiss();

            })

            .setNegativeButton(getString(R.string.cancel),
                DialogInterface.OnClickListener { dialog, which ->
                    dialog.dismiss()
                })

        dialog.create();
        dialog.show();
    }

    fun goProfile() {
        val eventParameters = "{\"Transition\":\"goProfile\"}"
        YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
        val intent = Intent(this, ProfileActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.profile_in,R.anim.activity_out)
    }

    fun goSettings() {
        val eventParameters = "{\"Transition\":\"goSettings\"}"
        YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
        val intent = Intent(this, SettingsActivity::class.java)
        startActivity(intent)
        overridePendingTransition(R.anim.activity_in,R.anim.activity_out)
    }

    override fun onStart() {
        AppCompatDelegate.setDefaultNightMode(
            if (sharedPreferences.getBoolean("dark_theme", false)) {
                AppCompatDelegate.MODE_NIGHT_YES
            } else {
                AppCompatDelegate.MODE_NIGHT_NO
            }
        )
        model.sortApps(sharedPreferences.getString("sort", getString(R.string.none)))
        val bitmap = Wallpaper.getBitmap(this)
        findViewById<ConstraintLayout>(R.id.main).background = BitmapDrawable(bitmap)
        Wallpaper.observers.add(observer)
        super.onStart()
        if (needHints) {
            showHints()
            needHints = false
        }

    }

    override fun onStop() {
        super.onStop()
        Wallpaper.observers.remove(observer)
    }

    override fun onDestroy() {
        super.onDestroy()
        unregisterReceiver(mReceiver)
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
//        MenuItemCompat.setOnActionExpandListener(menu?.findItem(R.id.action_search), this);
//        mSearch = menu!!.findItem(R.id.action_search)
//        mSearchView = mSearch.actionView as SearchView
//        mSearchView.setOnQueryTextListener(this)
//        mSearchView.isFocusable = true
//        mSearchView.isIconified = false

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // Handle item selection
        return when (item.itemId) {

            R.id.action_settings -> {
                goSettings()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateContextMenu(
        menu: ContextMenu?,
        v: View?,
        menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        super.onCreateContextMenu(menu, v, menuInfo)
        if ((v as CardView).tag is App) {
            selectedApp = (v?.tag as App)
            menu?.add(Menu.NONE, ADD_TO_DESKTOP, Menu.NONE, getString(R.string.add_to_desktop));
            menu?.add(Menu.NONE, ABOUT_APP, Menu.NONE, getString(R.string.about_app))
            menu?.add(Menu.NONE, OPEN_COUNT, Menu.NONE, getString(R.string.open_count))
            if (!selectedApp.isSystem) {
                menu?.add(Menu.NONE, DELETE_APP, Menu.NONE, getString(R.string.delete))
            }
            val eventParameters = "{\"ContextMenu\":\"create for ${selectedApp.label}\"}"
            YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
        }
        if ((v as CardView).tag is DesktopItem) {
            selectedDesktop = (v as CardView).tag as DesktopItem
            menu?.add(Menu.NONE, REMOVE_APP, Menu.NONE, getString(R.string.remove))
            val eventParameters = "{\"ContextMenu\":\"create for ${selectedDesktop.label}\"}"
            YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
        }
    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            ABOUT_APP -> aboutApp()
            DELETE_APP -> deleteApp()
            OPEN_COUNT -> openCount()
            REMOVE_APP -> removeFromMenu()
            ADD_TO_DESKTOP -> addToDesktop()
            else -> return super.onContextItemSelected(item)
        }
        return true
    }

    private fun removeFromMenu() {
        model.removeFromDesktop(selectedDesktop)
    }

    private fun addToDesktop() {
        val index = model.getFirstFreeIndex(0)
        if (index < MAX_COUNT)
            model.addToDesktop(AppDesktop(selectedApp, index))
        else {
            val snackbar = Snackbar.make(
                    mPager,
                    getString(R.string.not_enough_space),
                    Snackbar.LENGTH_LONG
            )
                    .setAction("Action", null)
            val snackView = snackbar.view
            snackView.setBackgroundResource(R.color.colorPrimaryDark)
            snackbar.show()
        }
    }

    private fun openCount() {
        val eventParameters = "{\"ContextMenu\":\"open count ${selectedApp.launchCount}\"}"
        YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
        val snackbar = Snackbar.make(
            mPager,
            getString(R.string.open_count) + ": " + selectedApp.launchCount,
            Snackbar.LENGTH_LONG
        )
            .setAction("Action", null)
        val snackView = snackbar.view
        snackView.setBackgroundResource(R.color.colorPrimaryDark)
        snackbar.show()
    }

    private fun aboutApp() {
        val eventParameters = "{\"ContextMenu\":\"about app ${selectedApp.packageName}\"}"
        YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
        try {
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            intent.setData(Uri.parse("package:" + selectedApp.packageName))
            startActivity(intent);
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
            val alternateIntent = Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS)
            startActivity(alternateIntent)
        }

    }

    private fun deleteApp() {
        val eventParameters = "{\"ContextMenu\":\"delete app ${selectedApp.packageName}\"}"
        YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
        val packageUri = Uri.parse("package:" + selectedApp.packageName);
        val uninstallIntent = Intent(Intent.ACTION_UNINSTALL_PACKAGE, packageUri);
        startActivity(uninstallIntent);
    }

    override fun onBackPressed() {
        if (mPager.currentItem == 0) {
            // If the user is currently looking at the first step, allow the system to handle the
            // Back button. This calls finish() on this activity and pops the back stack.
//            super.onBackPressed()

        } else {
            // Otherwise, select the previous step.

            mPager.currentItem = 0
        }
        mSearch.collapseActionView()
    }

    override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
        val eventParameters = "{\"Toolbar\":\"expand\"}"
        YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
        startSearchScreen = mPager.currentItem
        mPager.currentItem = 1
        gridListFragment.filter(mSearchView.query.toString())
        showKeyboardFrom(this)
        mSearchView.requestFocus()
        return true
    }

    override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
        val eventParameters = "{\"Toolbar\":\"collapse\"}"
        YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
        gridListFragment.filter("")
        mSearchView.setQuery("", false)
        hideKeyboardFrom(this, mSearchView)
        mPager.currentItem = startSearchScreen
        return true
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        mPager.currentItem = startSearchScreen
        hideKeyboardFrom(this, mSearchView)
        return true
    }

    override fun onQueryTextChange(p0: String?): Boolean {
        mPager.currentItem = 1
        gridListFragment.filter(p0)
        return true
    }

    fun hideKeyboardFrom(context: Context, view: View) {
        val imm: InputMethodManager =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showKeyboardFrom(context: Context) {
        val imm: InputMethodManager =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);

    }

    fun showHints() {
        val eventParameters = "{\"Hints\":\"show\"}"
        YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
        apps_menu_hint.visibility = View.VISIBLE
        apps_menu_hint.setOnTouchListener { view, motionEvent ->
            apps_menu_hint.visibility = View.GONE
            toolbar_hint.visibility = View.VISIBLE
            toolbar_hint.setOnTouchListener { view, motionEvent ->
                toolbar_hint.visibility = View.GONE
                toolbar_hint.setOnTouchListener(null)
                val eventParameters = "{\"Hints\":\"hide\"}"
                YandexMetrica.reportEvent(LAUNCHER_ACTIVITY_TAG, eventParameters)
                onStop()
                onStart()
                true
            }
            view.setOnTouchListener(null)
            true
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == WELCOME_ACTIVITY && resultCode == Activity.RESULT_OK) {
            sharedPreferences.edit().putBoolean("settingComplete", false).apply()

        }

    }

    override fun onClick(p0: View?) {
        val id = p0?.id
        when (id) {
            R.id.fab -> {
                animateFAB()
            }
            R.id.fab2 -> {
                val index = model.getFirstFreeIndex(0)
                if (index < MAX_COUNT)
                    addLink(index)
                else {
                    val snackbar: Snackbar = Snackbar.make(
                            mPager,
                            getString(R.string.not_enough_space),
                            Snackbar.LENGTH_LONG
                    )
                            .setAction("Action", null)
                    val snackView = snackbar.view
                    snackView.setBackgroundResource(R.color.colorPrimaryDark)
                    snackbar.show()
                }
            }
            R.id.fab1 -> {
                goProfile()
            }

        }
    }

    fun animateFAB() {

        if (isFabOpen) {

            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            isFabOpen = false;
            Log.d("Raj", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFabOpen = true;
            Log.d("Raj", "open");

        }
    }

}
