package com.example.zhevniak

import android.graphics.drawable.Drawable
import com.example.zhevniak.App
import com.example.zhevniak.DesktopItem
import com.example.zhevniak.ActivityStarter

class AppDesktop(val app: App, index:Int) : DesktopItem(index) {


    override var image: Drawable?
        get() = app.icon
        set(value) {}
    override var label: String
        get() = app.label
        set(value) {}





}