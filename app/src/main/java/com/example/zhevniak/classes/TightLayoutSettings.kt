package com.example.zhevniak.classes

class TightLayoutSettings {

    companion object {
        var layoutMode = LAYOUT.STANDARD
        enum class LAYOUT(val int: Int) {
            STANDARD(0),
            TIGHT(1)
        }
    }
}