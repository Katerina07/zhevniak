package com.example.zhevniak.classes

import android.graphics.drawable.Drawable

class AppItem(var appIcon: Drawable, var appName: CharSequence, var appDescription: CharSequence) {
}