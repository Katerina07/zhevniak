package com.example.zhevniak.custom

import android.app.Application
import android.content.Context
import androidx.appcompat.app.AppCompatDelegate

class CustomApplication: Application() {

    init {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }

    companion object {
        val sharedPreferencesFile = "MySharedPreferences"

        fun switchTheme(mode: Int) {
            AppCompatDelegate.setDefaultNightMode(mode)
        }
    }

}