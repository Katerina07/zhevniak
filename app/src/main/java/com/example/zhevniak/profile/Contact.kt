package com.example.zhevniak.profile

import android.graphics.drawable.Drawable
import android.view.View

data class Contact(val icon:Drawable, val title:String, val subtitle:String, val onClickListener: View.OnClickListener?) {

}
