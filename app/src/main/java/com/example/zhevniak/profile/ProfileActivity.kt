package com.example.zhevniak.profile

import android.app.Notification
import android.app.PendingIntent
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationManagerCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.zhevniak.CUSTOM_NOTIFICATION
import com.example.zhevniak.LauncherActivity
import com.example.zhevniak.R
import com.yandex.metrica.YandexMetrica
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.content_profile.*
import java.util.*


class ProfileActivity : AppCompatActivity() {

    var versionClick = 0
    lateinit var viewAdapter: ContactsAdapter
    lateinit var viewManager: LinearLayoutManager
    val PROFILE_ACTIVITY_TAG = "Profile activity"
    val sendMailOnClickListener = View.OnClickListener {
        val uri: Uri = Uri.parse("mailto:")
            .buildUpon()
            .build()

        val emailIntent = Intent(Intent.ACTION_SENDTO, uri)
        emailIntent.putExtra(Intent.EXTRA_EMAIL, arrayOf(getString(R.string.email)))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name))
        emailIntent.putExtra(Intent.EXTRA_TEXT, getString(R.string.feedback))
        startActivity(Intent.createChooser(emailIntent, getString(R.string.send_feedback)))
    }
    val phoneCallOnClickListener = View.OnClickListener {
        val eventParameters = "{\"Event\":\"phoneCall\"}"
        YandexMetrica.reportEvent(PROFILE_ACTIVITY_TAG, eventParameters)
        val intent =
            Intent(
                Intent.ACTION_DIAL,
                Uri.parse("tel:" + getString(R.string.developer_phone_number))
            )
        startActivity(intent)
    }
    val mapOnClickListener = View.OnClickListener {
        val eventParameters = "{\"Event\":\"mapCall\"}"
        YandexMetrica.reportEvent(PROFILE_ACTIVITY_TAG, eventParameters)
        val intent =
            Intent(
                Intent.ACTION_VIEW,
                Uri.parse("geo:" + getString(R.string.developer_map_coordinates))
            )
        startActivity(intent)
    }
    val gitOnClickListener = View.OnClickListener {
        val eventParameters = "{\"Event\":\"gitCall\"}"
        YandexMetrica.reportEvent(PROFILE_ACTIVITY_TAG, eventParameters)
        val intent =
            Intent(Intent.ACTION_VIEW, Uri.parse(getString(R.string.developer_gitlab)))
        startActivity(intent)
    }
    val versionOnClickListener = View.OnClickListener {
        if(versionClick == 6){
            versionClick = 0
            pushLove()
        }
        versionClick += 1
    }


    private fun pushLove() {
        val notificationIntent: Intent = Intent(this, LauncherActivity::class.java)
        val contentIntent = PendingIntent.getActivity(
                this,
                0, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT
        )
        val builder = Notification.Builder(this)
                .setContentTitle("What is love?")
                .setContentText("Baby don't hurt me")
                .setSmallIcon(R.drawable.ic_love)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_love))
                .setContentIntent(contentIntent)
                .setAutoCancel(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            builder
                    .setColor(this.getColor(R.color.colorSecondary))
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            builder.setChannelId(getString(R.string.channel_id))
        }
        val notificationManager =
                NotificationManagerCompat.from(this)
        notificationManager.notify(CUSTOM_NOTIFICATION, builder.build())
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        val eventParameters = "{\"OnCreate\":\"oncreate\"}"
        YandexMetrica.reportEvent(PROFILE_ACTIVITY_TAG, eventParameters)

        setSupportActionBar(toolbar)
        setting_fab.setOnClickListener(sendMailOnClickListener)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setTitle(R.string.developer_name)

        viewManager = LinearLayoutManager(this)

        viewAdapter = ContactsAdapter(getContacts(), resources.getColor(R.color.colorSecondary))
        contacts_view.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter

        }

    }

    private fun getContacts(): List<Contact> {
        val contacts = ArrayList<Contact>()
        contacts.add(
            Contact(
                resources.getDrawable(R.drawable.ic_email_black_24dp),
                getString(R.string.email),
                getString(R.string.email_name),
                sendMailOnClickListener
            )
        )
        contacts.add(
            Contact(
                resources.getDrawable(R.drawable.ic_local_phone_black_24dp),
                getString(R.string.developer_phone_number),
                getString(R.string.phone_name),
                phoneCallOnClickListener
            )
        )
        contacts.add(
            Contact(
                resources.getDrawable(R.drawable.ic_map_black_24dp),
                getString(R.string.developer_map),
                getString(R.string.map_name),
                mapOnClickListener
            )
        )
        contacts.add(
            Contact(
                resources.getDrawable(R.drawable.git_24dp),
                getString(R.string.developer_gitlab),
                getString(R.string.git_name),
                gitOnClickListener
            )
        )
        contacts.add(
                Contact(
                        resources.getDrawable(R.drawable.ic_info),
                        getString(R.string.version),
                        getString(R.string.versionNum),
                        versionOnClickListener
                )
        )
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        if(sharedPreferences.contains("silent_push")){
            contacts.add(0, Contact(
                resources.getDrawable(R.drawable.ic_info),
                sharedPreferences.getString("silent_push", ""),
                "silent push",
                null
            )
            )
        }
        return contacts
    }
    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.activity_in_main,R.anim.profile_out)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}
