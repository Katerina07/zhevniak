package com.example.zhevniak

import android.app.Application
import android.content.ContentResolver
import android.content.ContentValues
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import androidx.core.database.getLongOrNull
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.preference.PreferenceManager
import com.example.zhevniak.content.DbConstants
import com.yandex.metrica.YandexMetrica
import java.io.File


class AppsViewModel(application: Application) : AndroidViewModel(application) {
    val VIEW_MODEL_TAG = "AppsViewModel"
    val context = getApplication<Application>().applicationContext
    var currentSort = PreferenceManager.getDefaultSharedPreferences(context).getString(
        "sort",
        context.getString(R.string.none)
    )
    private val desktopLabels: MutableLiveData<List<DesktopItem>> =
        MutableLiveData<List<DesktopItem>>()
    private val apps: MutableLiveData<List<App>> = MutableLiveData<List<App>>().also {
        Thread { loadApps() }.start()
    }




    private lateinit var startApps: List<App>
    private val desktopLocal: MutableList<DesktopItem> = mutableListOf()

    fun getApps(): LiveData<List<App>> {
        return apps
    }



    fun getDesktopApps(): LiveData<List<DesktopItem>> {
        return desktopLabels
    }


    fun sortApps(sort: String) {
        val eventParameters = "{\"Sort apps\":\"$sort\"}"
        YandexMetrica.reportEvent(VIEW_MODEL_TAG, eventParameters)
        if (!currentSort.equals(sort)) {
            sortAppsForce(sort)
        }
    }

    fun sortAppsForce(sort: String) {
        val eventParameters = "{\"Sort apps force\":\"$sort\"}"
        YandexMetrica.reportEvent(VIEW_MODEL_TAG, eventParameters)
        if (sort == context.getString(R.string.none)) {
            apps.postValue(startApps)
            return
        }
        apps.postValue(startApps.sortedWith(when (sort) {
            context.getString(R.string.alphabet) -> Comparator<App> { a, b ->
                a.label.compareTo(
                    b.label
                )
            }
            context.getString(R.string.date) -> Comparator<App> { a, b -> (b.installDate - a.installDate).toInt() }
            context.getString(R.string.launch) -> Comparator<App> { a, b -> b.launchCount - a.launchCount }
            context.getString(R.string.reverse_alphabet) -> Comparator<App> { a, b ->
                -a.label.compareTo(
                    b.label
                )
            }

            else -> null

        } as Comparator<in App>))
        currentSort = sort
    }

    fun loadApps() {

        val packageManager = getApplication<Application>().packageManager
        startApps = packageManager.queryIntentActivities(Intent(Intent.ACTION_MAIN, null).apply {
            addCategory(Intent.CATEGORY_LAUNCHER)
        }, 0).toAppsArray(packageManager, getApplication<Application>().contentResolver)
        val eventParameters = "{\"Load apps\":\"${startApps.size}\"}"
        YandexMetrica.reportEvent(VIEW_MODEL_TAG, eventParameters)
        apps.postValue(startApps)
        sortAppsForce(
            currentSort
        )
        loadDesktop()
    }

    fun loadDesktop() {
        val uri =
            Uri.parse("content://com.example.zhevniak.provider/desktop")
        val cursor =
            getApplication<Application>().contentResolver.query(uri, null, null, null, null)
        cursor.moveToFirst()
        for (i in 1..cursor.count) {
            if (cursor.getString(cursor.getColumnIndex(DbConstants.COL_TYPE)).equals(DbConstants.TYPE_APP)) {
                val appPackage = cursor.getString(cursor.getColumnIndex(DbConstants.COL_INFO))
                var wasFound = false
                var index = 0
                while (!wasFound && index < startApps.size) {
                    if (startApps[index].packageName.equals(appPackage)) {
                        wasFound = true
                    } else
                        index++
                }
                if (wasFound) {
                    desktopLocal.add(
                        AppDesktop(
                            startApps[index],
                            cursor.getInt(cursor.getColumnIndex(DbConstants.COL_INDEX_DESKTOP))
                        )
                    )
                }
            } else {
                desktopLocal.add(
                    LinkDesktop(
                        cursor.getString(cursor.getColumnIndex(DbConstants.COL_LABEL)),
                        ColorDrawable(),
                        cursor.getString(cursor.getColumnIndex(DbConstants.COL_INFO)),
                        cursor.getInt(cursor.getColumnIndex(DbConstants.COL_INDEX_DESKTOP))
                    ).apply { ImageGetter.getImage(this.url, context, this, desktopLabels) }
                )

            }
            cursor.moveToNext()
        }

        val eventParameters = "{\"Load desktop\":\"${desktopLocal.size}\"}"
        YandexMetrica.reportEvent(VIEW_MODEL_TAG, eventParameters)
        desktopLabels.postValue(desktopLocal)

    }

    fun removeFromDesktop(item: DesktopItem) {
        desktopLocal.remove(item)
        desktopLabels.postValue(desktopLocal)
        context.contentResolver.delete(
            Uri.parse(DbConstants.DESKTOP_URI), "${DbConstants.COL_INDEX_DESKTOP}=?",
            arrayOf(item.index.toString())
        )
    }

    fun moveDesktop(index1: Int, index2: Int): Boolean {
        if (desktopLocal.firstOrNull { it.index == index2 } == null && desktopLocal.firstOrNull { it.index == index1 } != null) {
            val app = ContentValues()
            app.put(DbConstants.COL_INDEX_DESKTOP, index2)
            context.contentResolver.update(
                Uri.parse(DbConstants.DESKTOP_URI), app, "${DbConstants.COL_INDEX_DESKTOP}=?",
                arrayOf(index1.toString())
            )
            return true
        } else {
            return false
        }

    }

    fun addToDesktop(item: DesktopItem) {
        desktopLocal.add(item)
        desktopLabels.postValue(desktopLocal)
        val app = ContentValues()
        app.put(DbConstants.COL_LABEL, item.label)
        app.put(DbConstants.COL_INDEX_DESKTOP, item.index)
        if (item is AppDesktop) {
            app.put(DbConstants.COL_INFO, item.app.packageName)
            app.put(DbConstants.COL_TYPE, DbConstants.TYPE_APP)
        } else {
            if (item is LinkDesktop) {
                app.put(DbConstants.COL_INFO, item.url)
                app.put(DbConstants.COL_TYPE, DbConstants.TYPE_LINK)
            }
        }
        context.contentResolver.insert(Uri.parse(DbConstants.DESKTOP_URI), app)
    }

    fun updateApp(application: App) {
        val eventParameters = "{\"Update app\":\"${application.packageName}\"}"
        YandexMetrica.reportEvent(VIEW_MODEL_TAG, eventParameters)
        val app = ContentValues()
        app.put(DbConstants.COL_PACKAGE, application.packageName)
        app.put(DbConstants.COL_COUNT, application.launchCount)
        app.put(DbConstants.COL_LABEL, application.label)
        app.put(DbConstants.COL_LAST_START, application.lastLaunch)
        context.contentResolver.insert(Uri.parse(DbConstants.APPS_URI), app)
        sortAppsForce(currentSort)
    }

    fun getFirstFreeIndex(start: Int): Int {
        var answer = start
        desktopLocal.forEach {
            if (it.index == answer) {
                return getFirstFreeIndex(answer + 1)

            }
        }
        return answer
    }

}


private fun List<ResolveInfo>.toAppsArray(
    packageManager: PackageManager,
    contentResolver: ContentResolver
): List<App> {

    val apps = mutableListOf<App>()
    this.forEach {
        if (!it.activityInfo.packageName.equals("com.example.zhevniak")) {
            val uri =
                Uri.parse("content://com.example.zhevniak.provider/apps/" + it.activityInfo.packageName)
            val cursor = contentResolver.query(uri, null, null, null, null)
            cursor.moveToFirst()
            var count = 0
            var lastLaunch: Long? = null
            if (cursor.count != 0) {
                count = cursor.getInt(cursor.getColumnIndex(DbConstants.COL_COUNT))
                lastLaunch = cursor.getLongOrNull(cursor.getColumnIndex(DbConstants.COL_LAST_START))
            }
            apps.add(
                App(
                    it.loadLabel(packageManager).toString(),
                    it.activityInfo.packageName,
                    it.loadIcon(packageManager),
                    File(it.activityInfo.applicationInfo.dataDir).lastModified(),
                    count,
                    lastLaunch,
                    it.activityInfo.applicationInfo.flags and ApplicationInfo.FLAG_SYSTEM !== 0

                )
            )
        }
    }
    return apps
}
