package com.example.zhevniak.content

import android.provider.BaseColumns

object DbConstants {

    const val DESKTOP_URI = "content://com.example.zhevniak.provider/desktop"
    const val APPS_URI = "content://com.example.zhevniak.provider/apps"

    const val NAME = "zhevniakprovider.db"

    const val TABLE_APPS = "apps"
    const val TABLE_DESKTOP = "desktop"
    const val COL_ID = BaseColumns._ID

    const val COL_PACKAGE = "package"
    const val COL_COUNT = "count"
    const val COL_LAST_START = "date"
    const val COL_LABEL = "label"
    const val COL_INDEX_DESKTOP = "desktop"
    const val COL_TYPE = "type"
    const val COL_INFO = "info"
    const val CREATE_APPS = "CREATE TABLE $TABLE_APPS (" +
            "$COL_ID           INTEGER  PRIMARY KEY AUTOINCREMENT, \n" +
            "$COL_PACKAGE           TEXT, \n" +
            "$COL_LABEL      TEXT, \n" +
            "$COL_COUNT      INTEGER, \n" +
            "$COL_LAST_START      INTEGER\n" +
            ")"
    const val CREATE_DESKTOP = "CREATE TABLE $TABLE_DESKTOP (" +
            "$COL_ID           INTEGER  PRIMARY KEY AUTOINCREMENT, \n" +
            "$COL_TYPE           TEXT, \n" +
            "$COL_LABEL      TEXT, \n" +
            "$COL_INFO      TEXT, \n" +
            "$COL_INDEX_DESKTOP      INTEGER \n" +
            ")"
    const val DROP_APPS = "DROP TABLE IF EXISTS $TABLE_APPS"
    const val DROP_DESKTOP = "DROP TABLE IF EXISTS $TABLE_DESKTOP"

    const val TYPE_APP = "app"
    const val TYPE_LINK = "link"
}