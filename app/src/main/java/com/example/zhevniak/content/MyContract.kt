package com.example.zhevniak.content

import android.content.ContentResolver

object MyContract {

    const val AUTHORITY = "com.example.zhevniak.provider"

    object Apps {

        const val CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.example.zhevniak.provider_apps"
        const val CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.com.example.zhevniak.provider_apps"
    }


}