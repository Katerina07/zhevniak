package com.example.zhevniak.wallpaper

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.preference.PreferenceManager
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.zhevniak.ImageLoadService
import com.example.zhevniak.R
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.ImageRequest
import com.android.volley.toolbox.Volley
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream


object Wallpaper {
    val observers = mutableListOf<Observer<Bitmap?>>()
    val NONE = "none"
    val SITE_1 = "https://loremflickr.com"
    val SITE_2 = "https://picsum.photos"
    val HEIGHT = 1280
    val WIDTH = 720
    val PREFERENCES_KEY = "wallpaper_site"
    val PREFERENCES_TIME = "wallpaper_time"
    var currentSite = NONE
    var currentIntervalMinutes = 15
    var queue: RequestQueue? = null
    fun changeInterval(intervalInMinutes: Int, context: Context) {
        currentIntervalMinutes = intervalInMinutes
        AlarmManager.setAlarm(context, currentIntervalMinutes)
    }

    fun refreshIntervalMinutes(context: Context) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        currentIntervalMinutes = sp.getString(PREFERENCES_TIME, "0").toInt()
    }

    fun refreshSite(context: Context) {
        val sp = PreferenceManager.getDefaultSharedPreferences(context)
        currentSite = sp.getString(PREFERENCES_KEY, NONE)
    }

    fun changeSite(newSite: String, context: Context) {
        if (currentSite != newSite) {
            currentSite = newSite
            Log.d("AlarmReceiver", "changeSite")
            context.startService(
                Intent(context, ImageLoadService::class.java)
            )
        }
    }

    fun changeSite(newSite: String) {
        if (currentSite != newSite) {
            currentSite = newSite
        }
    }

    fun getBitmap(context: Context): Bitmap? {
        val file =
            File(context.filesDir, "wallpaper")
        if (!file.exists()) {
            return null
        }
        return BitmapFactory.decodeFile(file.absolutePath)
    }

    fun refresh(context: Context) {
        refreshSite(context)
        refreshIntervalMinutes(context)
        val file =
            File(context.filesDir, "wallpaper")
        if (!file.exists()) {
            file.createNewFile()
        }
        if (currentSite != NONE) {


            if (queue == null) {
                queue = Volley.newRequestQueue(context)
            }
            Log.d("AlarmReceiver", "request " + file.absoluteFile)

            var imageRequest: ImageRequest? = ImageRequest(
                "$currentSite/$WIDTH/$HEIGHT",  // Image URL
                Response.Listener { response ->
                    Log.d("AlarmReceiver", "response")
                    val bos = ByteArrayOutputStream()
                    response.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos)
                    val bitmapdata = bos.toByteArray()
                    val fos = FileOutputStream(file);
                    fos.write(bitmapdata);
                    fos.flush();
                    fos.close();
                    observers.forEach { it.onChanged(response) }
                },
                WIDTH,
                HEIGHT,
                ImageView.ScaleType.CENTER_CROP,
                Bitmap.Config.RGB_565,
                Response.ErrorListener {
                    Toast.makeText(
                        context,
                        context.getString(R.string.problem_loading_image),
                        Toast.LENGTH_LONG
                    ).show()
                }
            )
            queue?.cache?.clear()
            queue?.add(imageRequest)

        } else {
            file.delete()
            observers.forEach { it.onChanged(null) }
        }
    }
}