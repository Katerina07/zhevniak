package com.example.zhevniak.wallpaper

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.example.zhevniak.ImageLoadService
import java.text.Format
import java.text.SimpleDateFormat
import java.util.*


class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val msgStr = StringBuilder()
        val formatter: Format = SimpleDateFormat("hh:mm:ss a")
        msgStr.append(formatter.format(Date()))
        Log.d("AlarmReceiver", msgStr.toString())
        Wallpaper.refreshSite(context)
        context.startService(
            Intent(context, ImageLoadService::class.java)
        )
        Wallpaper.refreshIntervalMinutes(context)
        com.example.zhevniak.wallpaper.AlarmManager.setAlarm(context, Wallpaper.currentIntervalMinutes)
    }


}