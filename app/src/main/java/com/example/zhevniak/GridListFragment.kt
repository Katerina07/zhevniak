package com.example.zhevniak

import android.content.SharedPreferences
import android.content.res.Configuration
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.zhevniak.App
import com.example.zhevniak.AppsViewModel
import com.example.zhevniak.LauncherActivity

import com.example.zhevniak.R
import com.example.zhevniak.adapters.GridAdapter
import kotlinx.android.synthetic.main.activity_launcher.*
import kotlinx.android.synthetic.main.fragment_grid_list.*
import java.lang.Exception

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private lateinit var viewAdapter: GridAdapter
private lateinit var viewManager: RecyclerView.LayoutManager

/**
 * A simple [Fragment] subclass.
 * Use the [GridListFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GridListFragment : Fragment() {
    private val model: AppsViewModel by activityViewModels()

    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_grid_list, container, false)
    }

    private fun getColCount(): Int {
        val layout = sharedPreferences.getString("layout", getString(R.string.standard_layout))
        var colCount = 5;
        if (layout.equals(getString(R.string.standard_layout))) {
            colCount = 4
        }
        val orientation = activity?.getResources()?.getConfiguration()?.orientation
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            colCount += 2
        }
        return colCount
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        model.getApps().observe(viewLifecycleOwner, Observer<List<App>> { apps ->
            viewAdapter.wasChanged(apps)
            progressBar.isVisible = false
        })
    }

    override fun onStart() {
        super.onStart()
        viewManager = GridLayoutManager(context, getColCount())
        viewAdapter = GridAdapter(
            (activity as LauncherActivity),
            (activity as LauncherActivity).activityStarter
        )
        grid_recycler.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = viewAdapter
        }
        viewAdapter.wasChanged(model.getApps().value)

    }

    fun filter(label: String?) {
        if (label != null) {
           try{ val lowLabel = label.toLowerCase()
            viewAdapter.wasChanged(model.getApps().value?.filter {
                it.label.toLowerCase().contains(lowLabel)
            })}catch (e:Exception){
               e.printStackTrace()
           }
        }
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment GridListFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            GridListFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

}
